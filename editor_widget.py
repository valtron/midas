from PyQt5.QtCore import pyqtSignal as QSignal
from PyQt5.QtGui import QMouseEvent
from qutepart import Qutepart

class EditorWidget(Qutepart):
	on_mouse_released = QSignal(QMouseEvent)
	
	def __init__(self, font):
		super().__init__()
		self.indentUseTabs = True
		self.indentWidth = 2
		self.drawAnyWhitespace = True
		self.setFont(font)
		self.eol = '\n'
		self.completionEnable = False
	
	def mouseReleaseEvent(self, event):
		self.on_mouse_released.emit(event)
		event.accept()
