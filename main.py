from os.path import basename
from PyQt5.QtCore import QTimer, pyqtSignal as QSignal
from PyQt5.QtGui import QIcon, QColor, QFont, QFontMetrics, QTextOption, QCursor
from PyQt5.QtWidgets import (
	QMainWindow, QAction, QPushButton, QCheckBox, QProgressBar, QLabel,
	QComboBox, QCalendarWidget, QFileDialog, QMessageBox, QColorDialog,
	QStyleFactory, QApplication, QPlainTextEdit, QFontDialog,
	QHBoxLayout, QTabWidget, QWidget,
)
from models import Document, SettingSource
from editor_widget import EditorWidget

SETTINGS_FILE = '.midas'

def main():
	import sys
	ctrl = Ctrl()
	sys.exit(ctrl.run())

class Ctrl:
	def __init__(self):
		self.documents = []
		self.setting_sources = [
			SettingSource('~/{}'.format(SETTINGS_FILE), SettingSource.USER, "User"),
		]
		
		self.qapp = QApplication([])
		self.view = View(self)
	
	def has_unsaved_changes(self):
		for d in self.documents:
			if d.dirty: return True
		return False
	
	def new_document(self):
		doc = Document()
		self.documents.append(doc)
		return doc
	
	def open_document(self, path):
		doc = Document()
		doc.path = path
		doc.title = basename(path)
		self.documents.append(doc)
		return doc
	
	def edit_document(self, doc):
		doc.dirty = True
		self.view._ui_update()
	
	def save_document(self, doc, path):
		doc.dirty = False
		doc.path = path
		doc.title = basename(path)
		if doc.is_settings:
			self._reload_settings()
		self.view._ui_update()
	
	def close_document(self, doc):
		self.documents.remove(doc)
	
	def _reload_settings(self):
		# TODO
		pass
	
	def run(self):
		return self.qapp.exec_()

class View(QMainWindow):
	on_file_new = QSignal()
	on_file_open = QSignal()
	on_document_close = QSignal()
	on_settings_open = QSignal()
	on_document_save = QSignal()
	
	def __init__(self, ctrl):
		super().__init__()
		
		ctrl.qapp.aboutToQuit.connect(self._on_quit)
		self.ctrl = ctrl
		
		# Set up window
		layout = QHBoxLayout()
		layout.setContentsMargins(0, 0, 0, 0)
		cw = QWidget()
		cw.setLayout(layout)
		self.setCentralWidget(cw)
		self.setGeometry(300, 300, 500, 300)
		self.setWindowTitle("Midas Editor")
		self.setWindowIcon(QIcon('icon/text.png'))
		
		# Set props
		self.font = QFont("Consolas", 12)
		self.font.setFixedPitch(True)
		self.icon_swatch = QIcon('icon/swatch.png')
		self._tab2doc = {}
		
		# Set up UI
		self._setup_menu()
		self._setup_body(layout)
		self._setup_footer()
		self._ui_update()
		
		self.on_file_new.emit()
		
		self.show()
	
	def _setup_menu(self):
		menu = self.menuBar()
		
		fileMenu = menu.addMenu("&File")
		self._action(fileMenu, "&New", "Ctrl+N", "New", self._on_file_new)
		self._action(fileMenu, "&Open...", "Ctrl+O", "Open", self._on_file_open)
		self.saveAction = self._action(fileMenu, "&Save", "Ctrl+S", "Save", self._on_file_save)
		self.saveAsAction = self._action(fileMenu, "Save &As...", "Ctrl+Alt+S", "Save As...", self._on_file_save_as)
		fileMenu.addSeparator()
		self._action(fileMenu, "Exit", "Alt+F4", "Exit", self._on_exit)
		
		self.settingsMenu = menu.addMenu("&Settings")
	
	def _setup_body(self, layout):
		self.tabs = QTabWidget()
		layout.addWidget(self.tabs)
		self.tabs.setTabsClosable(True)
		self.tabs.tabCloseRequested.connect(self._on_file_close)
		self.tabs.currentChanged.connect(self._ui_update)
	
	def _setup_footer(self):
		self.statusBar()
	
	def _ui_update(self):
		# Full UI update
		
		tab = self.tabs.widget(self.tabs.currentIndex())
		save_as_enabled = False
		save_enabled = False
		if tab is not None:
			save_as_enabled = True
			save_enabled = (self._tab2doc[tab].path is not None)
		self.saveAction.setEnabled(save_enabled)
		self.saveAsAction.setEnabled(save_as_enabled)
		if not save_enabled and save_as_enabled:
			self.saveAsAction.setShortcut("Ctrl+S")
		else:
			self.saveAsAction.setShortcut("Ctrl+Alt+S")
		
		settingsMenu = self.settingsMenu
		settingsMenu.clear()
		for ss in self.ctrl.setting_sources:
			self._action(
				settingsMenu,
				"{} Settings...".format("User" if ss.type == SettingSource.USER else "[" + ss.title + "]"),
				None,
				"Edit settings in {}".format(ss.path),
				lambda: self._on_edit_settings(ss)
			)
		
		mp = QCursor.pos()
		
		tabbar = self.tabs.tabBar()
		for idx in range(self.tabs.count()):
			tab = self.tabs.widget(idx)
			doc = self._tab2doc[tab]
			tabbar.setTabTextColor(idx, QColor('red' if doc.dirty else 'black'))
			tabbar.setTabText(idx, doc.title)
			tabbar.setTabIcon(idx, self.icon_swatch)
	
	def _on_edit_settings(self, ss):
		# TODO: Open the file
		pass
	
	def _on_file_close(self, idx):
		tab = self.tabs.widget(idx)
		doc = self._tab2doc[tab]
		
		if doc.dirty:
			choice = QMessageBox.question(self,
				"Unsaved Changes", "Save \"{}\"?".format(doc.title),
				QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel
			)
			if choice == QMessageBox.Cancel:
				return
			if choice == QMessageBox.Save:
				self._on_file_save()
		
		self.ctrl.close_document(doc)
		
		del self._tab2doc[tab]
		
		tab.deleteLater()
		self.tabs.removeTab(idx)
	
	def _on_file_new(self):
		doc = self.ctrl.new_document()
		self._create_editor_tab(doc)
	
	def _on_file_open(self):
		(path, _) = QFileDialog.getOpenFileName(self, "Open File")
		if not path: return
		doc = self.ctrl.open_document(path)
		with open(path, 'r') as fh:
			text = fh.read()
		self._create_editor_tab(doc, text)
	
	def _on_file_save(self):
		idx = self.tabs.currentIndex()
		if idx < 0: return
		tab = self.tabs.widget(idx)
		doc = self._tab2doc[tab]
		if doc.path is None:
			self._on_file_save_as()
			return
		path = doc.path
		self._save_common(tab, doc.path)
	
	def _on_file_save_as(self):
		idx = self.tabs.currentIndex()
		if idx < 0: return
		(path, _) = QFileDialog.getSaveFileName(self, "Save As...")
		if not path: return
		tab = self.tabs.widget(idx)
		self._save_common(tab, path)
	
	def _save_common(self, tab, path):
		text = tab.toPlainText()
		with open(path, 'w') as fh:
			fh.write(text)
		doc = self._tab2doc[tab]
		self.ctrl.save_document(doc, path)
	
	def _create_editor_tab(self, doc, text = None):
		editor = EditorWidget(self.font)
		
		tab = editor
		self._tab2doc[tab] = doc
		
		idx = self.tabs.addTab(editor, doc.title)
		if text is not None:
			tab.setPlainText(text)
		
		self.tabs.setCurrentIndex(idx)
		self._ui_update()
		
		editor.textChanged.connect(lambda: self.ctrl.edit_document(doc))
		editor.on_mouse_released.connect(lambda ev: self._editor_click(ev, tab, doc))
	
	def _editor_click(self, ev, tab, doc):
		mp = ev.pos()
		c = tab.cursorForPosition(mp)
		b = c.block()
		p = c.positionInBlock()
		t = b.text()
		mn = max(p-3,0)
		print(t[mn:mn+6])
	
	def closeEvent(self, event):
		if not self.ctrl.has_unsaved_changes():
			return
		choice = QMessageBox.question(self, "Unsaved Changes",
			"You have unsaved changes. Are you sure you want to exit?",
			QMessageBox.Discard | QMessageBox.Cancel
		)
		if choice == QMessageBox.Cancel:
			event.ignore()
	
	def _on_exit(self):
		self.close()
	
	def _on_quit(self):
		# Clean up resources, if there are any
		pass
	
	def _action(self, menu, text, shortcut = None, tip = None, callback = None):
		action = QAction(text, self)
		if shortcut:
			action.setShortcut(shortcut)
		if tip:
			action.setStatusTip(tip)
		if callback:
			action.triggered.connect(callback)
		menu.addAction(action)
		return action
