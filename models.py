class Document:
	def __init__(self):
		self.path = None
		self.title = "<new>"
		self.dirty = False
		self.is_settings = False

class SettingSource:
	USER = 1
	PROJECT = 2
	
	def __init__(self, path, type, title):
		self.path = path
		self.type = type
		self.title = title
